import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-decorators',
  templateUrl: './decorators.component.html',
  styleUrls: ['./decorators.component.css']
})
export class DecoratorsComponent {
  onSubmit(user:any){
    console.log(user.value);
  }
  @Input()
  name: string | undefined;
}
