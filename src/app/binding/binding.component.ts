import { Component } from '@angular/core';

@Component({
  selector: 'app-binding',
  templateUrl: './binding.component.html',
  styleUrls: ['./binding.component.css']
})
export class BindingComponent {
   //String Interpolation
   name2="Prabhu"
   //Property Binding 
   load=false;
   constructor(){
     setTimeout(()=>{
       this.load=true;
     },2000)
   }
   // Event Binding
   name=""
   update(event:any){
     this.name=(<HTMLInputElement>event.target).value;
   }
   // Two way Binding
   modelname="Testing" 
}
