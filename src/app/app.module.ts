import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { StructureComponent } from './structure/structure.component';
import { BindingComponent } from './binding/binding.component';
import { DecoratorsComponent } from './decorators/decorators.component';
import { Binding2Component } from './binding2/binding2.component';

@NgModule({
  declarations: [
    AppComponent,
    StructureComponent,
    BindingComponent,
    DecoratorsComponent,
    Binding2Component,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
