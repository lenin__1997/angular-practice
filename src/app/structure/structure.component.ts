import { Component } from '@angular/core';

@Component({
  selector: 'app-structure',
  templateUrl: './structure.component.html',
  styleUrls: ['./structure.component.css']
})
export class StructureComponent {
  users = [{name:"Gopi",age:26,gender:'m'},
            {name:"Bharathi",age:29,gender:'f'},
          {name:"Praveen",age:24,gender:'m'},
        {name:"Amritha",age:26,gender:'f'},
      {name:"Shankar",age:29}]
}
